package br.com.galgo.notificadordependentes;

import br.com.galgo.notificadordependentes.model.JobExtracaoBaseCompromissoBO;
import br.com.galgo.notificadordependentes.model.ListaEmailProvedoresCompromissoBO;
import br.com.galgo.notificadordependentes.model.ServicoEnum;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class ReportManager {
    static Logger log = LoggerFactory.getLogger(ReportManager.class);

    // Delimiter used in CSV file
    private static final String NEW_LINE_SEPARATOR = "\n";

    // CSV file header
    private static final Object[] FILE_HEADER = { "Codigo Compromisso", "Data Geracao", "Status",
            "Nome da Entidade Provedora", "Papel da Entidade Provedora", "Nome da Entidade Consumidora",
            "Papel da Entidade Consumidora", "Servico", "CNPJ Fundo", "Codigo STI do Objeto", "Aprovadora" };

    // CSV file extended header
    private static final Object[] EXTENDED_FILE_HEADER = { "Codigo Compromisso", "Data Geracao", "Status",
            "Nome da Entidade Provedora", "Papel da Entidade Provedora", "Nome da Entidade Consumidora",
            "Papel da Entidade Consumidora", "Servico", "CNPJ Fundo", "Codigo STI do Objeto", "Aprovadora",
            "CNPJ Cotista", "Codigo STI do Cotista", "Cotista", "Data Inicio de Vigencia", "Data Fim de Vigencia" };

    // CSV file extended header
    private static final Object[] REPORTAUDIT_FILE_HEADER = { "Entidade", "Servico", "Quantidade", "Status e-mail",
            "Mailling Status" };

    private ReportManagerAudit reportAudit;
    private String mailMessage;

    /**
     * Construtor do report manager, o gerenciador de relatorio. Recebe como
     * parametro a mensagem padrao a ser enviada por email, alem da instancia do
     * mail sender, objeto que dispara os emails.
     * 
     * @param mailMessage a mensagem a ser enviada.
     * @author Eduardo.Calderini
     */
    public ReportManager(String mailMessage) {
        log.info("Criando instancia de ReportManager");
        this.mailMessage = mailMessage;

        // pega a instancia do relatorio de auditoria e limpa os dados.
        this.reportAudit = ReportManagerAudit.getInstance();
        this.reportAudit.destroy();
    }

    /**
     * Metodo que itera o mapa de objetos estruturados da classe
     * JobExtracaoBaseCompromissoBO, gera arquivo cvs e solicita o envido do
     * email para a intituicao.
     * 
     * @throws Exception caso algum erro ocorra.
     * @author Eduardo.Calderini
     */
    public void createSendReports() throws Exception {
        log.info("Enviando os relatórios com os arquivos anexados...");
        for (String baseCompromissoKey : JobExtracaoBaseCompromissoBO.getReportStrucureMap().keySet()) {

            String nomeInstituicao = JobExtracaoBaseCompromissoBO.getInstituicaoNome(baseCompromissoKey);
            ServicoEnum servico = JobExtracaoBaseCompromissoBO.getServicoEnum(baseCompromissoKey);
            log.debug("nomeInstituicao={}; servico={}", nomeInstituicao, servico);

            List<JobExtracaoBaseCompromissoBO> extracao2Report = JobExtracaoBaseCompromissoBO.getReportStrucureMap()
                    .get(baseCompromissoKey);

            String fileName = createReportBynameService(nomeInstituicao, servico, extracao2Report);
            log.debug("fileName={}", fileName);

            try {
                this.reportAudit.addElementInReport(nomeInstituicao, servico, extracao2Report.size());
                String subject = Controller.getInstance().getProperty(Controller.SUCESS_SUBJECT) + servico.toString();
                sendEmail(nomeInstituicao, servico, fileName, subject, null);
                this.reportAudit.updateStatus(nomeInstituicao, servico, fileName, true);
            } catch (Exception e) {
                log.error(null, e);
                this.reportAudit.updateStatus(nomeInstituicao, servico, fileName, false);
            }
        }

        String auditReportPath = this.reportAudit.createReport();
        String subject = Controller.getInstance().getProperty(Controller.SUBJECT_AUDIT_REPORT);
        URL url = ReportManager.class.getResource("/audit_mail_message.html");
        String message = FileUtils.readFileToString(FileUtils.toFile(url), "utf-8");
        sendEmail("Galgo Sistemas", null, auditReportPath, subject, message);
    }

    /**
     * Metodo que delega o disparo dos emails para a instancia do mailSender,
     * objeto parametrizado no construtor da classe.
     * 
     * @param nomeInstituicao o nome da instituicao que receberá o email
     * @param servico o servico que esta sendo tratado
     * @param strfilePath o caminho do arquivo csv do relatorio gerado
     * @author Eduardo.Calderini
     */
    private void sendEmail(String nomeInstituicao, ServicoEnum servico, String strfilePath, String psubject,
            String pMessage) {

        // message info
        String message = null;
        if (pMessage != null) {
            message = pMessage;
        } else {
            message = this.mailMessage;
        }

        // attachments, podemos ter mais de um.
        String[] attachFiles = new String[1];
        attachFiles[0] = strfilePath;

        try {
            List<String> emailslList = null;
            if (servico != null) {
                emailslList = ListaEmailProvedoresCompromissoBO.getMailListByEntidadeServico(nomeInstituicao, servico);

                if (emailslList == null || emailslList.isEmpty()) {
                    emailslList = (Arrays.asList(Controller.TO_ADDRESS_ERROR));
                    psubject = Controller.getInstance().getProperty(Controller.SUBJECT_MAIL_ADDRESS_NOT_FOUND)
                            + nomeInstituicao;
                    this.reportAudit.updateMaillingStatus(nomeInstituicao, servico, false);
                } else {
                    this.reportAudit.updateMaillingStatus(nomeInstituicao, servico, true);
                }
            } else {
                log.warn("Variável servico nula. "
                    + "Enviando e-mails para usuários de erro configurados em config.properties");
                emailslList = Arrays.asList(
                    Controller.getInstance().getProperty(Controller.ERROR_SEND_EMAILS_TO).split(","));
            }

            Controller.getInstance().getEmailAttachmentSender().sendEmailWithAttachments(emailslList, psubject, message,
                attachFiles);

            log.info("E-mail enviado com sucesso para {}", nomeInstituicao);
        } catch (Exception e) {
            String errorMsg = String.format("O e-mail nao pode ser enviado para a instituicao %s e servico %s.",
                nomeInstituicao, servico);
            log.error(errorMsg);
            throw new RuntimeException(e);
        }
    }

    
    /**
     * Metodo que cria o relatorio de servicos por instituicao.
     * 
     * @param nomeInstituicao o nome da instituicao
     * @param servico o servico para o qual deseja-se o relatorio
     * @param list a lista de compromissos pendentes da instuicao
     * @return retorna o nome do relatorio gerado
     * @throws Exception caso dê algum problema.
     */
    @SuppressWarnings("unused")
    @Deprecated
    private String oldCreateReportByNameService(String nomeInstituicao, ServicoEnum servico,
            List<JobExtracaoBaseCompromissoBO> list) throws Exception {
        log.info("Criando relatorio de {} para instituicao {}.", servico.toString(), nomeInstituicao);
        return createReportBynameService(nomeInstituicao, servico, list);
    }

    /**
     * 
     * @param nomeInstituicao
     * @param servico
     * @param list
     * @return
     * @throws Exception
     */
    private String createReportBynameService(String nomeInstituicao, ServicoEnum servico,
            List<JobExtracaoBaseCompromissoBO> list) throws Exception {

        Calendar now = GregorianCalendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        String formatedNow = dateFormat.format(now.getTime());

        String nomeEditado = nomeInstituicao.replace(" ", "_");
        String tempDir = Controller.getInstance().getProperty(Controller.TEMP_DIR);
        String fileName = tempDir + "/" + nomeEditado + "_" + servico + "_" + formatedNow + ".csv";

        try {
            OutputStream os = new FileOutputStream(fileName);

            /* A marca de ordem de byte (BOM) de arquivo UTF-8 */
            os.write(239);
            os.write(187);
            os.write(191);
            /***************************************************/

            PrintWriter w = new PrintWriter(new OutputStreamWriter(os, "UTF-8"));

            String fileHeader = "Codigo Compromisso;Data Geracao;Status;Nome da Entidade Provedora;Papel da Entidade Provedora;Nome da Entidade Consumidora;Papel da Entidade Consumidora;Servico;CNPJ Fundo;Codigo STI do Objeto;Aprovadora;CNPJ Cotista;Codigo STI do Cotista;Cotista;Data Inicio de Vigencia;Data Fim de Vigencia\r\n";
            w.print(fileHeader);

            // Write a new line object list to the CSV file
            for (JobExtracaoBaseCompromissoBO extracaoBO : list) {
                StringBuffer compromissoDataRecord = new StringBuffer();

                compromissoDataRecord.append(String.valueOf(extracaoBO.getCodigoCompromisso())); // A
                compromissoDataRecord.append(";");

                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // dd/MM/yyyy
                Date date = extracaoBO.getDataGeracao();
                compromissoDataRecord.append(sdfDate.format(date)); // B
                compromissoDataRecord.append(";");

                compromissoDataRecord.append(extracaoBO.getStatusCompromisso().toString()); // C
                compromissoDataRecord.append(";");

                // compromissoDataRecord.add(getEncodedString(extracaoBO.getNomeEntidadeProvedora()));
                // // D
                compromissoDataRecord.append(extracaoBO.getNomeEntidadeProvedora()); // D
                compromissoDataRecord.append(";");

                compromissoDataRecord.append(extracaoBO.getPapelEntidade().toString()); // E
                compromissoDataRecord.append(";");

                // compromissoDataRecord.add(
                // getEncodedString((extracaoBO.getNomeEntidadeConsumidora()))
                // );// F
                compromissoDataRecord.append(extracaoBO.getNomeEntidadeConsumidora()); // F
                compromissoDataRecord.append(";");
                log.debug("[NomeEntidadeConsumidora=" + extracaoBO.getNomeEntidadeConsumidora() + "]");

                // String papelEntidadeConsumidora =
                // extracaoBO.getPapelEntidadeConsumidora();
                // if()
                compromissoDataRecord.append(extracaoBO.getPapelEntidadeConsumidora().toString()); // G
                compromissoDataRecord.append(";");
                compromissoDataRecord.append(extracaoBO.getServico().toString()); // H
                compromissoDataRecord.append(";");

                compromissoDataRecord.append(extracaoBO.getCnpjFundo()); // J
                compromissoDataRecord.append(";");
                compromissoDataRecord.append(String.valueOf(extracaoBO.getCodigoSTI())); // K
                compromissoDataRecord.append(";");

                compromissoDataRecord.append(extracaoBO.getEntidadeAprovadora()); // X
                compromissoDataRecord.append(";");

                if (extracaoBO.getServico() == ServicoEnum.EXTRATO_DE_CONCILIACAO_DE_COTAS) {
                    // Coluna AC CNPJ Cotista sera usada
                    compromissoDataRecord.append(extracaoBO.getCnpjCotista());
                    compromissoDataRecord.append(";");

                    // AD
                    compromissoDataRecord.append(extracaoBO.getCodigoSTICotista());
                    compromissoDataRecord.append(";");

                    // AE
                    compromissoDataRecord.append(extracaoBO.getCotista());
                    compromissoDataRecord.append(";");
                } else {
                    // Coluna AC CNPJ Cotista sera usada
                    compromissoDataRecord.append("");
                    compromissoDataRecord.append(";");

                    // AD
                    compromissoDataRecord.append("");
                    compromissoDataRecord.append(";");

                    // AE
                    compromissoDataRecord.append("");
                    compromissoDataRecord.append(";");
                }

                // DATA INICIO DE VIGENCIA
                if (extracaoBO.getDataInicioVigencia() != null) {
                    date = extracaoBO.getDataInicioVigencia();
                    compromissoDataRecord.append(sdfDate.format(date).toString());
                    compromissoDataRecord.append(";");
                } else {
                    compromissoDataRecord.append("");
                    compromissoDataRecord.append(";");
                }

                // DATA FIM DE VIGENCIA
                if (extracaoBO.getDataFimVigencia() != null) {
                    date = extracaoBO.getDataFimVigencia();
                    compromissoDataRecord.append(sdfDate.format(date).toString());
                    compromissoDataRecord.append(";");
                } else {
                    compromissoDataRecord.append("");
                    compromissoDataRecord.append(";");
                }

                compromissoDataRecord.append("\r\n");
                w.print(compromissoDataRecord);
            }

            w.flush();
            w.close();
        } catch (FileNotFoundException e) {
            log.error(e.getMessage(), e);
            throw e;
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            throw e;
        }

        return fileName;
    }

    public Path getReportAuditPath() throws Exception {
        String strPath = ReportManagerAudit.getInstance().createReport();
        Path path = Paths.get(strPath);

        return path;
    }

    static class ReportManagerAudit {
        private static ReportManagerAudit myInstance;
        private HashMap<String, ReportManagerAudit> reportMap;

        String nomeEntidade;
        ServicoEnum servico;
        int qtdCompromissosPendentes;
        boolean statusEnvioEmail;
        private String reportKey;
        boolean statusMaillingList;

        private ReportManagerAudit() {
            reportMap = new HashMap<String, ReportManager.ReportManagerAudit>();
        }

        public void updateMaillingStatus(String nomeInstituicao, ServicoEnum servico, boolean existeMailling) {
            String key = nomeInstituicao + ":" + servico;

            if (reportMap.containsKey(key) == true) {
                ReportManagerAudit reportAudit = reportMap.get(key);
                reportAudit.statusMaillingList = existeMailling;
            }
        }

        public void updateStatus(String nomeInstituicao, ServicoEnum servico, String fileName, boolean statusEmail) {
            String key = nomeInstituicao + ":" + servico;

            if (reportMap.containsKey(key) == true) {
                ReportManagerAudit reportAudit = reportMap.get(key);
                reportAudit.statusEnvioEmail = statusEmail;
            }

        }

        public void destroy() {
            reportMap = new HashMap<String, ReportManager.ReportManagerAudit>();
            myInstance = null;
        }

        public static ReportManagerAudit getInstance() {
            if (myInstance == null) {
                myInstance = new ReportManagerAudit();
            }

            return myInstance;
        }

        public String createReport() throws Exception {
            log.info("Criando relatorio de auditoria.");

            FileWriter fileWriter = null;
            CSVPrinter csvFilePrinter = null;

            // Create the CSVFormat object with "\n" as a record delimiter
            CSVFormat csvFileFormat = CSVFormat.newFormat(';').withRecordSeparator(NEW_LINE_SEPARATOR);

            Calendar now = GregorianCalendar.getInstance();
            DateFormat dateFormat = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
            String formatedNow = dateFormat.format(now.getTime());

            String temDir = Controller.getInstance().getProperty(Controller.TEMP_DIR);
            String nomeRelatorioAuditoria = Controller.getInstance().getProperty(Controller.NOME_RELATORIO_AUDITORIA);
            String fileName = temDir + "/" + nomeRelatorioAuditoria + "_" + formatedNow + ".csv";

            try {
                // initialize FileWriter object
                fileWriter = new FileWriter(fileName);

                // initialize CSVPrinter object
                csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

                // Create CSV file header
                csvFilePrinter.printRecord(REPORTAUDIT_FILE_HEADER);

                // Write a new line object list to the CSV file
                for (Entry<String, ReportManagerAudit> pair : reportMap.entrySet()) {
                    List<String> outputLine = new ArrayList<String>();
                    ReportManagerAudit reportAudit = pair.getValue();

                    outputLine.add(reportAudit.nomeEntidade);
                    outputLine.add(reportAudit.servico.toString());
                    outputLine.add(String.valueOf(reportAudit.qtdCompromissosPendentes));
                    outputLine.add(String.valueOf(reportAudit.statusEnvioEmail));
                    outputLine.add(String.valueOf(reportAudit.statusMaillingList));

                    csvFilePrinter.printRecord(outputLine);
                }

                log.info("CSV de auditoria criado com sucesso: {}.", fileName);
            } catch (Exception e) {
                log.error("Erro ao escrever arquivo CSV.", e);

                throw e;
            } finally {
                try {
                    fileWriter.flush();
                    fileWriter.close();
                    csvFilePrinter.close();
                } catch (IOException e) {
                    log.error("Erro ao escrever arquivo CSV.", e);

                    throw e;
                }
            }

            return fileName;
        }

        public void addElementInReport(String nomeInstituicao, ServicoEnum servico, int qtdCompromissosPendentes) {
            ReportManagerAudit rptAudit = new ReportManagerAudit();
            rptAudit.reportKey = getReportKey(nomeInstituicao, servico);
            rptAudit.nomeEntidade = nomeInstituicao;
            rptAudit.servico = servico;
            rptAudit.qtdCompromissosPendentes = qtdCompromissosPendentes;
            rptAudit.statusEnvioEmail = false;

            addInReportAudit(rptAudit);
        }

        public void addInReportAudit(ReportManagerAudit rptAudit) {
            if (reportMap.containsKey(rptAudit.reportKey) == true) {
                // se o relatorio ja existe, substitui.
                reportMap.put(rptAudit.reportKey, rptAudit);
            } else {
                reportMap.put(rptAudit.reportKey, rptAudit);
            }
        }

        private String getReportKey(String nomeInstituicao, ServicoEnum servico) {
            return nomeInstituicao + ":" + servico;
        }

    }

}