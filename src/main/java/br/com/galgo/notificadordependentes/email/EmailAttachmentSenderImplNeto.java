package br.com.galgo.notificadordependentes.email;

import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;


public class EmailAttachmentSenderImplNeto implements EmailAttachmentSender {
    private static final Logger log = LoggerFactory.getLogger(EmailSenderImplNeto.class);
    private final HtmlEmail email;
    private static final List<CompletableFuture> futures = new ArrayList<>();

    /**
     * Construtor da classe de email.
     * Este construtor cinstancia e configura as propriedades necessarias para o envio de email.
     * As propriedades sao: o host utilizado
     *                      a porta utilizada
     *                      neste caso consideramos a obrigatoriedade de autenticacao
     *                      o nome do usuario
     *                      a senha do usuario
     *                      se será ativado em modo de debug ou nao
     * @param host o host a ser utilziado  para envio SMTP
     * @param port a porta a ser utilizada para envio SMTP
     * @param userName o usuario que da conta SMTP
     * @param password a senha da conta SMTP
     * @param isDebugMode se sera usado em modo de debug ou nao
     */
    public EmailAttachmentSenderImplNeto(final String host, final String port,
        final String userName, final String password,
        final boolean isDebugMode) {
        email = new HtmlEmail();
        email.setDebug(isDebugMode);
        email.setHostName(host);
        email.setSmtpPort(Integer.parseInt(port));
        email.setAuthenticator(new DefaultAuthenticator(userName, password));
        email.setStartTLSEnabled(true);
        try {
            email.setFrom(userName);
        } catch (EmailException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void sendEmailWithAttachments(List<String> toAddress,
        String subject, String message, String[] attachFiles) throws MessagingException {
        if (attachFiles == null || attachFiles.length == 0) {
            throw new IllegalArgumentException("Deve haver no mínimo 1 anexo para utilizar este método");
        }
        try {
            email.setSubject(subject);
            email.setHtmlMsg(message);
            email.addTo(toAddress.toArray(new String[]{}));
            for (String fileName : attachFiles) {
                email.attach(Paths.get(fileName).toFile());
            }
            CompletableFuture future = CompletableFuture.runAsync(() -> {
                try {
                    log.info("Enviando e-mail com {} anexos para {}...", attachFiles.length, toAddress);
                    email.send();
                    log.info("E-mail enviado!");
                } catch (EmailException e) {
                    e.printStackTrace();
                }
            });
            log.debug("future = {}, subject = {}", future, subject);
            futures.add(future);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void waitEmails() throws ExecutionException, InterruptedException {
        log.info("Lista de processos de envio de e-mail sem anexo com {} elementos", futures.size());
        log.info("Aguardando a conclusão destes processos...");
        CompletableFuture.allOf((CompletableFuture<?>[]) futures.toArray(new CompletableFuture[]{})).get();
    }
}