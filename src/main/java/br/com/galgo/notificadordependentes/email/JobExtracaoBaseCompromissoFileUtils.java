package br.com.galgo.notificadordependentes.email;

import br.com.galgo.notificadordependentes.Controller;
import br.com.galgo.notificadordependentes.model.JobExtracaoBaseCompromissoBO;
import br.com.galgo.notificadordependentes.model.JobExtracaoBaseCompromissoStatusEnum;
import br.com.galgo.notificadordependentes.model.ListaEmailProvedoresCompromissoBO;
import br.com.galgo.notificadordependentes.model.PapelEntidadeComunidoraEnum;
import br.com.galgo.notificadordependentes.model.PapelEntidadeProvedoraEnum;
import br.com.galgo.notificadordependentes.model.ServicoEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

import static br.com.galgo.notificadordependentes.model.JobExtracaoBaseCompromissoStatusEnum.valueOf;

public class JobExtracaoBaseCompromissoFileUtils {
    static Logger log = LoggerFactory.getLogger(JobExtracaoBaseCompromissoFileUtils.class);

    private static final String FILE_ENCODING = "Cp1252";
    private static final String CVS_SPLITBY = ";";


    /**
     * Metodo que processa o arquivo CSV STIPRDB_Job_Extracao_Base_Compromisso
     * Como premissa o arquivo tem que estar com encoding Cp1252 e separado por ;
     * Ao ler o arquivo, um filtro por status de compromisso é aplicado, sendo considerado apenas os seguintes status
     *      SOLICITACAO_AGUARDANDO_APROVACAO
     *      SOLICITACAO_AGUARDANDO_APROVACAO_ALTERACAO_PROPOSTA
     *      SOLICITACAO_AGUARDANDO_CONFIRMACAO_ALTERACAO
     *      SOLICITACAO_AGUARDANDO_VALIDACAO
     *  Além do filtro por status, nem todas as colunas sao relevantes no processamento do arquivo, sendo 
     *  assim, é levado em consideracao somente as colunas: A a H, J, K, X, AC a AE
     *  As colunas de AC a AE, só são preenchidas no arquivo quando o a coluna de serviço for do tipo EXTRATO_DE_CONCILIACAO_DE_COTAS.
     * @param filePath Path do arquivo a ser lido.
     * @throws IOException
     * @throws ParseException
     */
    public static void processFile(Path filePath) throws IOException, ParseException {
        log.info("Dando inicio ao processamento do job de compromissos'");

        String csvFile = filePath.toString();
        BufferedReader br = null;
        String line = "";

        try {
            //br = new BufferedReader(new FileReader(csvFile));
            br = new BufferedReader(
                new InputStreamReader(new FileInputStream(csvFile), JobExtracaoBaseCompromissoFileUtils.FILE_ENCODING));
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                if (count == 1) {
                    //pula a linha de header do arquivo
                    continue;
                }

                //split da linha 
                String[] splitedCurrentLine = line.split(CVS_SPLITBY);
                               
                
                /* existem status no arquivo que nao interessam, vamos filtrar por:
                 * SOLICITACAO_AGUARDANDO_APROVACAO
                 * SOLICITACAO_AGUARDANDO_APROVACAO_ALTERACAO_PROPOSTA
                 * SOLICITACAO_AGUARDANDO_CONFIRMACAO_ALTERACAO
                 * SOLICITACAO_AGUARDANDO_VALIDACAO
                 * SOLICITACAO_AGUARDANDO_APROVACAO_REATIVACAO
                 * SOLICITACAO_AGUARDANDO_CONFIRMACAO_SUSPENSAO
                 */
                JobExtracaoBaseCompromissoStatusEnum statusCompromisso = valueOf(
                    splitedCurrentLine[2]);
                final String[] aFormalizarStatusList = Controller.getInstance().getProperty("A_FORMALIZAR_STATUS_LIST").split(
                    ",");
                if (Stream.of(aFormalizarStatusList).map(JobExtracaoBaseCompromissoStatusEnum::valueOf).
                    anyMatch(status ->  statusCompromisso.equals(status))) {

                    JobExtracaoBaseCompromissoBO extracaoBO = new JobExtracaoBaseCompromissoBO();

                    //*****************************************************************
                    // As colunas que farão parte do CSV são: A a H, J, K, X, AC a AE *
                    //*****************************************************************

                    // Coluna A, B, C D, E, F, G e H  farao parte do relatorio 
                    extracaoBO.setCodigoCompromisso(Integer.parseInt(splitedCurrentLine[0])); //A
                    extracaoBO.setDataGeracao(convertString2Date(splitedCurrentLine[1], "dd/MM/yyyy HH:mm")); //B
                    extracaoBO.setStatusCompromisso(statusCompromisso); //C
                    extracaoBO.setNomeEntidadeProvedora(splitedCurrentLine[3]); //D
                    extracaoBO.setPapelEntidade(PapelEntidadeProvedoraEnum.getAsEnum(splitedCurrentLine[4])); //E 
                    extracaoBO.setNomeEntidadeConsumidora(splitedCurrentLine[5]);//F 
                    extracaoBO.setPapelEntidadeConsumidora(PapelEntidadeComunidoraEnum.getAsEnum(splitedCurrentLine[6])); //G

                    //TODO ecalderini adicionar no javadoc
                    if (ServicoEnum.getAsEnum(splitedCurrentLine[7]) == ServicoEnum.INFORMACOES_ANBIMA) {
                        extracaoBO.setServico(ServicoEnum.PL_COTA); //H
                    } else {
                        extracaoBO.setServico(ServicoEnum.getAsEnum(splitedCurrentLine[7])); //H
                    }

                    // Coluna I será ignorada
                    //extracaoVO.setTipoCompromisso(TipoCompromissoEnum.getAsEnum(splitedCurrentLine[8]));

                    // Coluna J CNPJ do fundo fara parte do relatorio
                    extracaoBO.setCnpjFundo(splitedCurrentLine[9]); // J
                    // Coluna K Codigo STI fara parte do relatorio
                    extracaoBO.setCodigoSTI(Integer.parseInt(splitedCurrentLine[10])); // K

                    //Colunas L, M, N, O, P, Q, R, S, T, U, V e W serao ignoradas
                    //extracaoVO.setMoeda(splitedCurrentLine[11]);
                    extracaoBO.setDataInicioVigencia(convertString2Date(splitedCurrentLine[12], "dd/MM/yyyy"));
                    extracaoBO.setDataFimVigencia(convertString2Date(splitedCurrentLine[13], "dd/MM/yyyy"));
                    //extracaoVO.setHora(splitedCurrentLine[14]);
                    //extracaoVO.setPrazoLiberacao(splitedCurrentLine[15]);
                    //extracaoVO.setPeriodicidade(splitedCurrentLine[16]);
                    //extracaoVO.setDataBase(splitedCurrentLine[17]);
                    //extracaoVO.setDataEnvio(splitedCurrentLine[18]);
                    //extracaoVO.setCustoRecebimento(splitedCurrentLine[19]);
                    //extracaoVO.setCustoEnvio(splitedCurrentLine[20]);
                    //extracaoVO.setDataInicioSuspensao(convertString2Date(splitedCurrentLine[21], "dd/MM/yyyy"));
                    //extracaoVO.setDataFimSuspensao(convertString2Date(splitedCurrentLine[22],"dd/MM/yyyy"));

                    //Coluna X Entidade aprovadora.
                    extracaoBO.setEntidadeAprovadora(splitedCurrentLine[23]); // X

                    //Colunas Y e Z serao ignoradas
                    //extracaoVO.setNivelAprovacao(splitedCurrentLine[24]);
                    //extracaoVO.setSolicitante(splitedCurrentLine[25]);

                    //Couluna AA será ignorada.
                    //extracaoVO.setPapelSolicitante(splitedCurrentLine[26]);

                    // As colunas abaixo, AC, AD e AE so serao usadas para servico de EXTRATO_DE_CONCILIACAO_DE_COTAS 
                    if (extracaoBO.getServico() == ServicoEnum.EXTRATO_DE_CONCILIACAO_DE_COTAS) {
                        //Coluna AC CNPJ Cotista sera usada 
                        extracaoBO.setCnpjCotista(splitedCurrentLine[28]);

                        // AD
                        extracaoBO.setCodigoSTICotista(splitedCurrentLine[29]);

                        // AE
                        extracaoBO.setCotista(splitedCurrentLine[30]);
                    }

                    JobExtracaoBaseCompromissoBO.addInReportStructure(extracaoBO);
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
            throw e;
        } catch (IOException e) {
            log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
            throw e;
        } catch (ParseException e) {
            log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
            throw e;
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    log.error("Erro ao processar job de compromisso: " + filePath.toString(), e);
                }
            }
        }

        log.info("Arquivo processado com sucesso: " + filePath.toString());
    }


    /**
     * Metodo que converte uma string data em um objeto do tipo data de acordo com o formato desejado.
     * @param date2Contert a data em formato de string que se deseja converter
     * @param format a string de formato que se deseja a data
     * @return um objeto do tipo Data formatado
     * @throws ParseException caso nao consegua parsear a data parametrizada.
     * @author Eduardo.Calderini
     */
    private static Date convertString2Date(String date2Contert, String format) throws ParseException {
        if (date2Contert == null || date2Contert.equals("")) {
            return null;
        }

        DateFormat formatter = new SimpleDateFormat(format);
        Date date = (Date) formatter.parse(date2Contert);

        return date;
    }


    /**
     * Metodo que lê o arquivo de emails, recebe como parametro uma string com o caminho do arquivo.
     * @param filePath o caminho completdo do arquivo
     * @throws IOException caso algum erro de IO aconteca.
     */
    public static void readEmailsFile(String filePath) throws IOException {

        ListaEmailProvedoresCompromissoBO.clearStatic();
        String csvFile = filePath;
        String line = "";
        String csvSplitBy = ";";

        try (BufferedReader br = new BufferedReader(
            new InputStreamReader(new FileInputStream(csvFile), JobExtracaoBaseCompromissoFileUtils.FILE_ENCODING))) {
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                if (count == 1) {
                    continue;
                }
                String nomeEntidadeProvedora = "";
                try {
                    String[] splitedCurrentLine = line.split(csvSplitBy);
                    nomeEntidadeProvedora = splitedCurrentLine[0];
                    ServicoEnum servicoProvido = ServicoEnum.getAsEnum(splitedCurrentLine[1]);
                    String strEmail = splitedCurrentLine[2];
                    ListaEmailProvedoresCompromissoBO.addEmailEntidadeProvedora(
                        nomeEntidadeProvedora, servicoProvido, strEmail);
                } catch (Exception e) {
                    log.info(
                        "Serviço ou e-mail não encontrado para Entidade: " + nomeEntidadeProvedora + " na linha: "
                            + count);
                    continue;
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Erro ao processar job de compromisso: " + filePath, e);
            throw e;
        } catch (IOException e) {
            log.error("Erro ao processar job de compromisso: " + filePath, e);
            throw e;
        }
        log.info("Arquivo processado com sucesso: " + filePath);
    }
}
