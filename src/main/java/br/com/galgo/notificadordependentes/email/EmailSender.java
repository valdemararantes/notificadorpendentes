package br.com.galgo.notificadordependentes.email;

import java.util.List;

/**
 * Created by Valdemar.Arantes on 12/08/2016.
 */
public interface EmailSender {
    void sendEmail(List<String> toAddress, String subject, String message);
}
