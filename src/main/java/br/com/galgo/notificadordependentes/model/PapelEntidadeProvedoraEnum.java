package br.com.galgo.notificadordependentes.model;

public enum PapelEntidadeProvedoraEnum {
    CONTROLADOR_DE_ATIVOS,
    CONTROLADOR_DE_PASSIVO;
    
    public static PapelEntidadeProvedoraEnum getAsEnum(String str2Convert)
    {
        if(str2Convert.equals("CONTROLADOR DE ATIVOS"))
        {
            return CONTROLADOR_DE_ATIVOS;
        }
        
        if(str2Convert.equals("CONTROLADOR DE PASSIVO"))
        {
            return CONTROLADOR_DE_PASSIVO;
        }
        
        return null;
    }

}
