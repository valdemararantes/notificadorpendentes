package br.com.galgo.notificadordependentes.model;

public enum ServicoEnum {
    PL_COTA,
    POSICAO_DE_ATIVOS_DA_CARTEIRA,
    INFORMACOES_ANBIMA,
    EXTRATO_DE_CONCILIACAO_DE_COTAS;


    
    
    public static ServicoEnum getAsEnum(String str2Convert)
    {
        if(str2Convert.equals("PL/COTA") || str2Convert.equals("PL_COTA"))
        {
            return PL_COTA;
        }

        if(str2Convert.equals("POSICAO DE ATIVOS DA CARTEIRA") || str2Convert.equals("POSICAO_DE_ATIVOS_DA_CARTEIRA"))
        {
            return POSICAO_DE_ATIVOS_DA_CARTEIRA;
        }

        if(str2Convert.equals("INFORMACOES ANBIMA") || str2Convert.equals("INFORMACOES_ANBIMA"))
        {
            return INFORMACOES_ANBIMA;
        }

        if(str2Convert.equals("EXTRATO DE CONCILIACAO DE COTAS") || str2Convert.equals("EXTRATO_DE_CONCILIACAO_DE_COTAS"))
        {
            return EXTRATO_DE_CONCILIACAO_DE_COTAS;
        }
        
        return null;
    }
    
}
