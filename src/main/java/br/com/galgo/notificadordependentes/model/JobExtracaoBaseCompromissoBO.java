package br.com.galgo.notificadordependentes.model;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

import br.com.galgo.notificadordependentes.email.JobExtracaoBaseCompromissoFileUtils;

public class JobExtracaoBaseCompromissoBO {

    private int codigoCompromisso;
    private Date dataGeracao;
    private JobExtracaoBaseCompromissoStatusEnum statusCompromisso;
    private String nomeEntidadeProvedora;
    private PapelEntidadeProvedoraEnum papelEntidade;
    private String nomeEntidadeConsumidora;
    private PapelEntidadeComunidoraEnum papelEntidadeConsumidora;
    private ServicoEnum servico;
    private TipoCompromissoEnum tipoCompromisso;
    private String cnpjFundo;
    private int codigoSTI;
    private String moeda;
    private Date dataInicioVigencia;
    private Date dataFimVigencia;
    private String hora;
    private String prazoLiberacao;
    private String periodicidade;
    private String dataBase;
    private String dataEnvio;
    private String custoRecebimento;
    private String custoEnvio;
    private Date dataInicioSuspensao;
    private Date dataFimSuspensao;
    private String entidadeAprovadora;
    private String nivelAprovacao;
    private String solicitante;
    private String papelSolicitante;
    private String observacoesMotivo;
    private String cnpjCotista;
    private String codigoSTICotista;
    private String cotista;

    /*
     * Metodos e variaveis estaticos
     */
    static Logger logger = LoggerFactory.getLogger(JobExtracaoBaseCompromissoBO.class);

    private static HashMap<String, List<JobExtracaoBaseCompromissoBO>> reportStrucureMap;

    /**
     * Metodo que cria a instancia do mapa estruturado de objetos de extacao
     * base compromisso se necessario e retorna sua instancia
     * 
     * @return uma instancia do mapa estruturado de objetos de extacao base
     *         compromisso
     * @author Eduardo.Calderini
     */
    public static HashMap<String, List<JobExtracaoBaseCompromissoBO>> getReportStrucureMap() {
        if (reportStrucureMap == null) {
            logger.info("Criando instancia do mapa estruturado de JobExtracaoBaseCompromissoBO");

            reportStrucureMap = new HashMap<String, List<JobExtracaoBaseCompromissoBO>>();
        }

        return reportStrucureMap;
    }

    /**
     * Adicioana uma objeto do tipo JobExtracaoBaseCompromissoBO ao mapa
     * estruturado de objetos de extacao base compromisso.
     * 
     * @param extracaoBO
     *            objeto do tipo JobExtracaoBaseCompromissoBO para ser
     *            adicionado ao mapa estruturado de objetos de extacao base
     *            compromisso.
     * @author Eduardo.Calderini
     */
    public static void addInReportStructure(JobExtracaoBaseCompromissoBO extracaoBO) {
        String keyMap = extracaoBO.getEntidadeAprovadora() + ":" + extracaoBO.getServico();

        logger.info("Criando chave para o mapa estruturado de JobExtracaoBaseCompromissoBO " + keyMap);

        if (getReportStrucureMap().containsKey(keyMap) == true) {
            getReportStrucureMap().get(keyMap).add(extracaoBO);

            logger.info("[CHAVE ENCONTRADA]" + keyMap + " tamanho lista: " + getReportStrucureMap().get(keyMap).size());

        } else {
            logger.info("[NOVA CHAVE]" + keyMap);

            List<JobExtracaoBaseCompromissoBO> jobExtracaoBaseCompromissoBO = new ArrayList<JobExtracaoBaseCompromissoBO>();
            jobExtracaoBaseCompromissoBO.add(extracaoBO);
            getReportStrucureMap().put(keyMap, jobExtracaoBaseCompromissoBO);
        }
    }

    /**
     * Metodo que limpa o mapa estatico.
     * 
     * @author Eduardo.Calderini
     */
    private static void clearStatic() {
        reportStrucureMap = null;
    }

    /**
     * Metodo que recebe a chave composta do mapa de compromissos, efetua o
     * parser e devolve o nome da instituicao.
     * 
     * @param baseCompromissoKey
     *            a cahve composta
     * @return o nome da instituicao
     * @throws Exception
     *             caso de algum erro no parser do nome.
     * @author Eduardo.Calderini
     */
    public static String getInstituicaoNome(String baseCompromissoKey) throws Exception {
        try {
            String[] splitedKey = splitCompostKey(baseCompromissoKey);

            if (splitedKey.length != 2) {
                return null;
            }

            return splitedKey[0];
        } catch (Exception e) {
            logger.info("Nao foi possivel parserar a chave do mapa estruturado " + baseCompromissoKey);

            throw e;
        }
    }

    /**
     * Metodo que recebe a chave composta do mapa de comprimissos, efetua o
     * parser e devolve tipo de servico
     * 
     * @param baseCompromissoKey
     *            a cahve composta
     * @return o servico associado a instituicao
     * @throws Exception
     *             caso de algum erro no parser do nome.
     * @author Eduardo.Calderini
     */
    public static ServicoEnum getServicoEnum(String baseCompromissoKey) throws Exception {
        try {
            String[] splitedKey = splitCompostKey(baseCompromissoKey);

            if (splitedKey.length != 2) {
                return null;
            }

            return ServicoEnum.getAsEnum(splitedKey[1]);
        } catch (Exception e) {
            logger.info("Nao foi possivel parserar a chave do mapa estruturado " + baseCompromissoKey);

            throw e;
        }
    }

    /**
     * Metodo que recebe a chave composta e aplica o split, retornado um array
     * com nome da instituicao e compromisso.
     * 
     * @param baseCompromissoKey
     *            a cahve composta
     * @return um array com nome da instituicao e compromisso.
     * @throws Exception
     *             caso de algum erro no parser do nome.
     * @author Eduardo.Calderini
     */
    private static String[] splitCompostKey(String baseCompromissoKey) throws Exception {
        try {
            if (baseCompromissoKey == null) {
                return null;
            }

            if (baseCompromissoKey.isEmpty() == true) {
                return null;
            }

            return baseCompromissoKey.split(":");
        } catch (Exception e) {
            throw e;
        }
    }

    
    public static void processFile(Path file2Process) throws IOException, ParseException {
        JobExtracaoBaseCompromissoBO.clearStatic();
        JobExtracaoBaseCompromissoFileUtils.processFile(file2Process);
    }
    
    
    /*
     * getters e setters de classe
     */
    public int getCodigoCompromisso() {
        return codigoCompromisso;
    }

    public void setCodigoCompromisso(int codigoCompromisso) {
        this.codigoCompromisso = codigoCompromisso;
    }

    public Date getDataGeracao() {
        return dataGeracao;
    }

    public void setDataGeracao(Date dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    public JobExtracaoBaseCompromissoStatusEnum getStatusCompromisso() {
        return statusCompromisso;
    }

    public void setStatusCompromisso(JobExtracaoBaseCompromissoStatusEnum statusCompromisso) {
        this.statusCompromisso = statusCompromisso;
    }

    public String getNomeEntidadeProvedora() {
        return nomeEntidadeProvedora;
    }

    public void setNomeEntidadeProvedora(String nomeEntidadeProvedora) {
        this.nomeEntidadeProvedora = nomeEntidadeProvedora;
    }

    public PapelEntidadeProvedoraEnum getPapelEntidade() {
        return papelEntidade;
    }

    public void setPapelEntidade(PapelEntidadeProvedoraEnum papelEntidade) {
        this.papelEntidade = papelEntidade;
    }

    public String getNomeEntidadeConsumidora() {
        return nomeEntidadeConsumidora;
    }

    public void setNomeEntidadeConsumidora(String nomeEntidadeConsumidora) {
        this.nomeEntidadeConsumidora = nomeEntidadeConsumidora;
    }

    public PapelEntidadeComunidoraEnum getPapelEntidadeConsumidora() {
        return papelEntidadeConsumidora;
    }

    public void setPapelEntidadeConsumidora(PapelEntidadeComunidoraEnum papelEntidadeConsumidora) {
        this.papelEntidadeConsumidora = papelEntidadeConsumidora;
    }

    public ServicoEnum getServico() {
        return servico;
    }

    public void setServico(ServicoEnum servico) {
        this.servico = servico;
    }

    public TipoCompromissoEnum getTipoCompromisso() {
        return tipoCompromisso;
    }

    public void setTipoCompromisso(TipoCompromissoEnum tipoCompromisso) {
        this.tipoCompromisso = tipoCompromisso;
    }

    public String getCnpjFundo() {
        return cnpjFundo;
    }

    public void setCnpjFundo(String cnpjFundo) {
        this.cnpjFundo = cnpjFundo;
    }

    public int getCodigoSTI() {
        return codigoSTI;
    }

    public void setCodigoSTI(int codigoSTI) {
        this.codigoSTI = codigoSTI;
    }

    public String getMoeda() {
        return moeda;
    }

    public void setMoeda(String moeda) {
        this.moeda = moeda;
    }

    public Date getDataInicioVigencia() {
        return dataInicioVigencia;
    }

    public void setDataInicioVigencia(Date dataInicioVigencia) {
        this.dataInicioVigencia = dataInicioVigencia;
    }

    public Date getDataFimVigencia() {
        return dataFimVigencia;
    }

    public void setDataFimVigencia(Date dataFimVigencia) {
        this.dataFimVigencia = dataFimVigencia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getPrazoLiberacao() {
        return prazoLiberacao;
    }

    public void setPrazoLiberacao(String prazoLiberacao) {
        this.prazoLiberacao = prazoLiberacao;
    }

    public String getPeriodicidade() {
        return periodicidade;
    }

    public void setPeriodicidade(String periodicidade) {
        this.periodicidade = periodicidade;
    }

    public String getDataBase() {
        return dataBase;
    }

    public void setDataBase(String dataBase) {
        this.dataBase = dataBase;
    }

    public String getDataEnvio() {
        return dataEnvio;
    }

    public void setDataEnvio(String dataEnvio) {
        this.dataEnvio = dataEnvio;
    }

    public String getCustoRecebimento() {
        return custoRecebimento;
    }

    public void setCustoRecebimento(String custoRecebimento) {
        this.custoRecebimento = custoRecebimento;
    }

    public String getCustoEnvio() {
        return custoEnvio;
    }

    public void setCustoEnvio(String custoEnvio) {
        this.custoEnvio = custoEnvio;
    }

    public Date getDataInicioSuspensao() {
        return dataInicioSuspensao;
    }

    public void setDataInicioSuspensao(Date dataInicioSuspensao) {
        this.dataInicioSuspensao = dataInicioSuspensao;
    }

    public Date getDataFimSuspensao() {
        return dataFimSuspensao;
    }

    public void setDataFimSuspensao(Date dataFimSuspensao) {
        this.dataFimSuspensao = dataFimSuspensao;
    }

    public String getEntidadeAprovadora() {
        return entidadeAprovadora;
    }

    public void setEntidadeAprovadora(String entidadeAprovadora) {
        this.entidadeAprovadora = entidadeAprovadora;
    }

    public String getNivelAprovacao() {
        return nivelAprovacao;
    }

    public void setNivelAprovacao(String nivelAprovacao) {
        this.nivelAprovacao = nivelAprovacao;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public String getPapelSolicitante() {
        return papelSolicitante;
    }

    public void setPapelSolicitante(String papelSolicitante) {
        this.papelSolicitante = papelSolicitante;
    }

    public String getObservacoesMotivo() {
        return observacoesMotivo;
    }

    public void setObservacoesMotivo(String observacoesMotivo) {
        this.observacoesMotivo = observacoesMotivo;
    }

    public String getCnpjCotista() {
        return cnpjCotista;
    }

    public void setCnpjCotista(String cnpjCotista) {
        this.cnpjCotista = cnpjCotista;
    }

    public String getCodigoSTICotista() {
        return codigoSTICotista;
    }

    public void setCodigoSTICotista(String codigoSTICotista) {
        this.codigoSTICotista = codigoSTICotista;
    }

    public String getCotista() {
        return cotista;
    }

    public void setCotista(String cotista) {
        this.cotista = cotista;
    }

}
