package br.com.galgo.notificadordependentes.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class ListaEmailProvedoresCompromissoBO {

    /**
     * Mapa de instancias. Uma insntancia por instituicao.
     */
    private static HashMap<String, ListaEmailProvedoresCompromissoBO> mapOfInstances; 
    
    
    private String entidadeProvedora;
    private HashMap<ServicoEnum, List<String>> mapaServicoEmails;
    
    
    /**
     * Construtor da classe.
     * @param entidadeProvedora recebe o nome de  uma entidade provedora.
     * @author Eduardo.Calderini
     */
    private ListaEmailProvedoresCompromissoBO(String entidadeProvedora)
    {
        this.entidadeProvedora = entidadeProvedora;
    }
    
    
    /**
     * Metodo que retorna o cria e retorna o mapa de instancias de ListaEmailProvedoresCompromissoBO
     * @return o mapa de instancias de ListaEmailProvedoresCompromissoBO
     * @author Eduardo.Calderini
     */
    private static HashMap<String, ListaEmailProvedoresCompromissoBO> getMapOfInstances()
    {
        if(mapOfInstances == null)
        {
            // se nao existe a instancia, cria.
            mapOfInstances = new HashMap<String, ListaEmailProvedoresCompromissoBO>();
        }
        
        return mapOfInstances;
    }
    
    
    /**
     * Metodo que retorna uma instancia de ListaEmailProvedoresCompromissoBO de acordo com o nome da entidade.
     * @param nomeEntidadeProvedora o nome da entidade que se deseja objter a instancia.
     * @return uma instancia de ListaEmailProvedoresCompromissoBO de acordo com o nome da entidade.
     * @author Eduardo.Calderini
     */
    public static ListaEmailProvedoresCompromissoBO getInstanceByEntidadeProvedora(String nomeEntidadeProvedora)
    {
        if(getMapOfInstances().containsKey(nomeEntidadeProvedora) == true)
        {
            return getMapOfInstances().get(nomeEntidadeProvedora);
        }
        
        ListaEmailProvedoresCompromissoBO newInstance = new ListaEmailProvedoresCompromissoBO(nomeEntidadeProvedora);
        getMapOfInstances().put(nomeEntidadeProvedora, newInstance);
        return newInstance;
    }
    
    
    /**
     * Metodo que retorna a lista de emails de uma determinada entidade associada a um determinado servico
     * Retorna uma lista vazia caso nao tenha registrado emails para o servico especificado para a entidade.
     * @param nomeEntidadeProvedora o nome da entidade
     * @param servico o servico da entidade.
     * @return a lista de email, caso exista. Retorna uma lista vazia caso contrario.
     * @author Eduardo.Calderini
     */
    public static List<String> getMailListByEntidadeServico(String nomeEntidadeProvedora, ServicoEnum servico)
    {
        ListaEmailProvedoresCompromissoBO entity = getInstanceByEntidadeProvedora(nomeEntidadeProvedora);
        
        if(entity != null)
        {
            HashMap<ServicoEnum, List<String>> mapaEmails = entity.getMapaServicoEmails();
            if (mapaEmails.containsKey(servico) == true)
            {
                return mapaEmails.get(servico);
            }
        }
        
        return new ArrayList<String>();
    }
    
    
    /**
     * Meotodo que retorna o mapa de emails por servico.
     * @return o mapa de emails por servico.
     * @author Eduardo.Calderini
     */
    private HashMap<ServicoEnum, List<String>> getMapaServicoEmails()
    {
        if(mapaServicoEmails == null)
        {
            mapaServicoEmails = new HashMap<ServicoEnum, List<String>>();
        }
        
        return mapaServicoEmails;
    }
    
    
    /**
     * Adiciona um email a lista de emails por servico.
     * @param email2Add o email para ser adicionado a lista de emails.
     * @param serviceReference o servico de referencia.
     */
    public void addEmailByService(String email2Add, ServicoEnum serviceReference)
    {
        //service de referencia ja existe
        if(getMapaServicoEmails().containsKey(serviceReference) == true)
        {
            List<String> emailList = getMapaServicoEmails().get(serviceReference);
            
            //se email nao estiver duplicado adiciona.
            if(emailList.contains(email2Add) == false)
            {
                emailList.add(email2Add);
            }
            
        } else 
        {
            List<String> emailList = new ArrayList<String>();
            emailList.add(email2Add);
            
            getMapaServicoEmails().put(serviceReference, emailList);
        }
    }
    
    
    /**
     * Retorna o nome da entidade provedora.
     * @return o nome da entidade provedora.
     * @author Eduardo.Calderini
     */
    public String getEntidadeProvedora() 
    {
        return entidadeProvedora;
    }

    
    /**
     * Seta o nome da entidade provedora
     * @param entidadeProvedora o nome da entidade provedora
     * @author Eduardo.Calderini
     */
    public void setEntidadeProvedora(String entidadeProvedora) 
    {
        this.entidadeProvedora = entidadeProvedora;
    }


    /**
     * Adiciona ao mapa de entidades o email de acordo com o servico e a entidade provedora
     * @param nomeEntidadeProvedora o nome da entidade provedora
     * @param servicoProvido o servico de referencia
     * @param strEmail o email a ser adicionado
     * @author Eduardo.Calderini
     */
    public static void addEmailEntidadeProvedora(String nomeEntidadeProvedora, ServicoEnum servicoProvido,
            String strEmail) {
        
        ListaEmailProvedoresCompromissoBO myInstance = getInstanceByEntidadeProvedora(nomeEntidadeProvedora);
        myInstance.addEmailByService(strEmail, servicoProvido);
    }


    /**
     * Meotodo que limpa a lista de instancias.
     * @author Eduardo.Calderini
     */
    public static void clearStatic() 
    {
        mapOfInstances = null;
    }
    
}