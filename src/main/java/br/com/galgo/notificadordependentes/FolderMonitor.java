package br.com.galgo.notificadordependentes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Esta classe não é mais utilizada neste projeto. A mantive aqui apenas como um modelo de monitoração de pasta e arquivo.
 */
public class FolderMonitor {

    static Logger logger = LoggerFactory.getLogger(FolderMonitor.class);

    private static final String JOB_EXTRACAO_BASE_COMPROMISSO_FILE = "STIPRDB_Job_Extracao_Base_Compromisso_";
    private static final String JOB_EXTRACAO_BASE_COMPROMISSO_MIME = "csv";

    private Path pastaMonitorada;

    /**
     * Construtor default da classe, monitora a pasta parametrizado no
     * Controller.PATH_TESTE_FOLDER com os seguintes eventos:
     * StandardWatchEventKinds.ENTRY_CREATE StandardWatchEventKinds.ENTRY_MODIFY
     * 
     * @throws IOException
     *             caso haja algum problema de monitoracao
     * @author Eduardo.Calderini
     */
    private FolderMonitor() {
        // void
    }

    /**
     * Construtor da classe, recebe uma pasta à ser monitorada e monitora com os
     * seguintes eventos: StandardWatchEventKinds.ENTRY_CREATE
     * StandardWatchEventKinds.ENTRY_MODIFY
     * 
     * @throws IOException
     *             caso haja algum problema de IO
     * @author Eduardo.Calderini
     */
    public FolderMonitor(String folder2Monitor) throws IOException {
        logger.info("Instanciando Folder Monitor. Pasta monitorada: " + folder2Monitor);

        pastaMonitorada = Paths.get(folder2Monitor);
    }

    /**
     * Metodo que monitora a pasta parametrizada no construtor.
     * 
     * @author Eduardo.Calderini
     * @throws Exception
     * @throws ParseException
     */
    void doMonitor() throws ParseException, Exception {
        logger.info("Iniciando monitoracao. Pasta monitorada: " + pastaMonitorada.toString());

        final Map<WatchKey, Path> registroPastasMonitoradas = new HashMap<>();
        final WatchService monitor = FileSystems.getDefault().newWatchService();

        // eventos q serão monitorados
        final WatchKey monitorEventos = pastaMonitorada.register(monitor, StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.OVERFLOW);

        // link watched folder with events to be listened
        registroPastasMonitoradas.put(monitorEventos, pastaMonitorada);

        int count = 0;
        while (true) {
            WatchKey watchKey = null;
            try {
                logger.debug("Esperando evendo de pasta ***********");
                watchKey = monitor.take();
                logger.debug("Esperando evendo capturado");
            } catch (InterruptedException e) {
                logger.error("Um erro de monitoracao ocorreu", e);
                throw e;
            }

            // pasta monitorada
            Path pasta = registroPastasMonitoradas.get(watchKey);

            logger.info("Registrando pasta para monitoracao: " + pasta);

            if (pasta == null) {
                logger.error("A Pasta registrada nao pode ser monitorada " + pasta);

                continue;
            }

            for (WatchEvent<?> event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = pastaMonitorada.resolve(name);

                // imprime o evento
                logger.info("Evento capturado: {}: {}\n", event.kind().name(), child);

                // com o evento correto em uma pasta valida iniciamos a
                // monitoracao do arquivo
                if (validaNomePastaIdentificada(child) == true) {
                    doMonitorFile(child);
                    logger.debug("[DOMONITOR-pasta] retornando de  doMonitorFile:" + count);
                    return;
                }

                boolean valid = monitorEventos.reset();
                if (!valid) {
                    registroPastasMonitoradas.remove(watchKey);
                    if (registroPastasMonitoradas.isEmpty()) {
                        break;
                    }
                }
            }

            logger.debug("[DOMONITOR-pasta] contando " + count++);
        }
    }

    /**
     * Método que efetua a monitoracao do arquivo de Job Extracao Base
     * Compromisso.
     * 
     * @param fullFilePath o caminho completo do arquivo que sera monitorado
     * @author Eduardo.Calderini
     * @throws Exception
     * @throws ParseException
     */
    static void doMonitorFile(Path fullFilePath) throws ParseException, Exception {
        logger.info("Iniciando monitoracao do arquivo. Arquivo monitorado: " + fullFilePath.toString());

        Path arquivoMonitorado = fullFilePath;
        WatchService monitor = FileSystems.getDefault().newWatchService();
        Map<WatchKey, Path> registroPastasMonitoradas = new HashMap<WatchKey, Path>();

        // eventos q serão monitorados
        // WatchKey setMonitorEventos = pastaMonitorada.register(monitor,
        // StandardWatchEventKinds.ENTRY_CREATE,
        // StandardWatchEventKinds.ENTRY_MODIFY);
        WatchKey setMonitorEventos = arquivoMonitorado.register(monitor, StandardWatchEventKinds.ENTRY_CREATE,
                StandardWatchEventKinds.ENTRY_MODIFY);

        // link watched folder with events to be listened
        registroPastasMonitoradas.put(setMonitorEventos, arquivoMonitorado);

        int count = 0;
        while (true) {
            WatchKey watchKey = null;
            try {
                logger.info("Esperando evendo de arquivo ***********");
                watchKey = monitor.take();
                logger.info("Esperando evendo capturado");
            } catch (InterruptedException e) {
                logger.error("Um erro de monitoracao ocorreu", e);

                throw e;
            }

            // arquivo monitorado
            Path arquivo = registroPastasMonitoradas.get(watchKey);

            logger.info("Registrando arquivo para monitoracao: " + arquivo.toString());

            for (WatchEvent<?> event : watchKey.pollEvents()) {
                WatchEvent.Kind kind = event.kind();
                if (kind == StandardWatchEventKinds.OVERFLOW) {
                    continue;
                }

                WatchEvent<Path> ev = cast(event);
                Path name = ev.context();
                Path child = arquivoMonitorado.resolve(name);

                // imprime o evento
                logger.info("Evento Capturado:  {}: {}", event.kind().name(), child);

                // TODO melhorar nome arquivo
                if (validaNomeArquivoMonitorado(child) == true) {

                    waitFileLoad(child);

                    Controller.getInstance().processJobExtracaoBaseCompromisso(child);

                    // se ja foi processado o arquivo, remove da monitoracao.
                    registroPastasMonitoradas.remove(watchKey);
                    return;

                    /*
                     * if (registroPastasMonitoradas.isEmpty()) { break; }
                     */
                }

                boolean valid = setMonitorEventos.reset();
                if (!valid) {
                    registroPastasMonitoradas.remove(watchKey);
                    if (registroPastasMonitoradas.isEmpty()) {
                        break;
                    }
                }
            }

            logger.debug("[DOMONITOR-arquivo] contando " + count++);
        }

    }

    /**
     * Metodo que verifica de tempo em tempo o tamanho do arquivo parametrizado.
     * O tempo de espera está parametrizado em Controller.WAIT_TIME_LOAD_FILE A
     * validação é feita pelo variacao do tanho do arquivo. Assume-se que quando
     * nao há mais varção de tamanho é porque o arquivo terminou de carregar.
     * 
     * @param child
     *            o caminho do arquivo que se deseja monitorar.
     * @throws InterruptedException
     * @author eduardo.calderini
     */
    private static void waitFileLoad(Path child) throws InterruptedException {
        long lengthAtual = 0;
        long lengthAnterior = 0;
        boolean waitFileLoad = true;
        while (waitFileLoad) {
            lengthAnterior = lengthAtual;
            long waitTime = Long.parseLong(Controller.getInstance().getProperty(Controller.WAIT_TIME_LOAD_FILE));
            Thread.sleep(waitTime);
            File f = new File(child.toString());
            lengthAtual = f.length();

            if (lengthAnterior == lengthAtual) {
                waitFileLoad = false;
            }
        }
    }

    /**
     * Metodo que verifica se o arquivo parametrizado é o job extracao base
     * compromisso
     * 
     * @param filePath
     *            o arquivo que se deseja validar
     * @return true, caso arquivo valido. False caso contrario.
     * @author Eduardo.Calderini
     */
    private static boolean validaNomeArquivoMonitorado(Path filePath) {
        // STIPRDB_Job_Extracao_Base_Compromisso_20160422.csv
        String pattern = Pattern.quote(File.separator);
        String[] splitedFilePath = filePath.toString().split(pattern);

        if (splitedFilePath != null && splitedFilePath.length >= 2) {
            String fileName = splitedFilePath[splitedFilePath.length - 1];
            String folderName = splitedFilePath[splitedFilePath.length - 2];

            if (isValidFilename(fileName, folderName) == true) {
                return true;
            }
        }

        return false;
    }

    /**
     * Metodo que valida o nome do arquivo de job extracao base compromisso.
     * 
     * @param fileName
     *            o nome do arquivo de job extracao base compromisso.
     * @param folderName
     *            a pasta em que o arquivo se encontra
     * @return true caso valido, false caso contrario
     * @author Eduardo.Calderini
     */
    private static boolean isValidFilename(String fileName, String folderName) {
        // o nome do arquivo tem que ser deste tipo
        // STIPRDB_Job_Extracao_Base_Compromisso_20160422.csv

        if (fileName.length() != 50) {
            return false;
        }

        if (fileName.substring(0, 38).equals(JOB_EXTRACAO_BASE_COMPROMISSO_FILE) == false) {
            return false;
        }

        if (fileName.substring(fileName.length() - 3).equals(JOB_EXTRACAO_BASE_COMPROMISSO_MIME) == false) {
            return false;
        }

        if (fileName.substring(38, fileName.length() - 4).equals(folderName) == false) {
            return false;
        }

        return true;
    }

    /**
     * Metodo que valida o nome da pasta criada dentro da pasta monitorada
     * 
     * @param child
     *            o caminho completo da pasta que se deseja validar
     * @return true caso valido, false caso contrario
     * @author Eduardo.Calderini
     */
    private static boolean validaNomePastaIdentificada(Path child) {
        String pattern = Pattern.quote(File.separator);
        String[] splitedPath = child.toString().split(pattern);
        boolean isValidFolder = true;

        if (splitedPath != null && splitedPath.length > 0) {

            // esperamos uma pasta com nome no padrao aaaammdd: 20160428
            String strNomePasta = splitedPath[splitedPath.length - 1];

            isValidFolder = isValidFolder && (strNomePasta.length() == 8) ? true : false;
            isValidFolder = isValidFolder && isNumeric(strNomePasta) ? true : false;
            isValidFolder = isValidFolder && isDateFormatValid(strNomePasta) ? true : false;
        }

        return isValidFolder;
    }

    /**
     * Valida se o nome da pasta parametrizada segue o formato yyyyMMdd
     * 
     * @param strNomePasta
     *            o nome a ser validado
     * @return true caso sim, false caso contrario.
     * @author eduardo.calderini
     */
    private static boolean isDateFormatValid(String strNomePasta) {
        try {
            new SimpleDateFormat("yyyyMMdd").parse(strNomePasta);
        } catch (Exception ex) {
            return false;
        }

        return true;
    }

    /**
     * Metodo que valida se a string parametrizada é um numero
     * 
     * @param str2Validate
     *            a string que se deseja validar
     * @return true caso valido, false caso contrario
     */
    private static boolean isNumeric(String str2Validate) {
        return str2Validate.matches("[+-]?\\d*(\\.\\d+)?");
    }

    /**
     * 
     * @param event
     * @return
     */
    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

}
