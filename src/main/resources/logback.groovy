//
// Built on Fri May 06 18:17:18 UTC 2016 by logback-translator
// For more information on configuration files in Groovy
// please see http://logback.qos.ch/manual/groovy.html

// For assistance related to this tool or configuration files
// in general, please contact the logback user mailing list at
//    http://qos.ch/mailman/listinfo/logback-user

// For professional support please see
//   http://www.qos.ch/shop/products/professionalSupport
import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.classic.filter.ThresholdFilter
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender

import java.nio.charset.Charset

// Add status listener, which prints out
// the Logback configuration on application startup.
//statusListener OnConsoleStatusListener

def bySecond = timestamp("yyyyMMdd'T'HHmmss")
appender("STDOUT", ConsoleAppender) {
    def idea = System.getProperty('idea.launcher.bin.path')
    def rebel = System.getProperty('rebel.env.ide')
    def javaCommand = System.getProperty("sun.java.command");
    def useConsole = System.getProperty("use.console")

    encoder(PatternLayoutEncoder) {
        if (useConsole) {
            charset = Charset.forName("windows-1252")
        }else if (!idea && !rebel && !javaCommand.contains("intellij")) {
            println "Logback utiliza charset=cp850"
            charset = Charset.forName("cp850")
        } else {
            println "Logback utiliza charset=UTF-8"
            charset = Charset.forName("UTF-8")
        }
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} [%M] - %m .\\(%class{0}.java:%line\\)%n"
    }
}

appender("FILE", FileAppender) {
    file = "./log/log-${bySecond}.txt"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{5} [%M] - %m .\\(%class{0}.java:%line\\)%n"
    }
}

appender("FILE_WARN", FileAppender) {
    file = "./log/warn-${bySecond}.txt"
    filter (ThresholdFilter) {
        level = WARN
    }
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%d{HH:mm:ss.SSS} %m%n"
    }
}

logger("javax", INFO, [], false)
logger("org", INFO, [], true)
logger("com.galgo.cxfutils.sec", INFO, [], true)

root(DEBUG, ["STDOUT", "FILE_WARN", "FILE"])
