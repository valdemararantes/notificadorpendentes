package br.com.galgo.notificadordependentes.email;

import br.com.galgo.notificadordependentes.Controller;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;


public class EmailAttachmentSenderImpl implements EmailAttachmentSender {
    private String userName;
    private Session session;

    /**
     * Construtor da classe de email.
     * Este construtor cinstancia e configura as propriedades necessarias para o envio de email.
     * As propriedades sao: o host utilizado 
     *                      a porta utilizada 
     *                      neste caso consideramos a obrigatoriedade de autenticacao
     *                      o nome do usuario
     *                      a senha do usuario
     *                      se será ativado em modo de debug ou nao 
     * @param host o host a ser utilziado  para envio SMTP
     * @param port a porta a ser utilizada para envio SMTP
     * @param userName o usuario que da conta SMTP
     * @param password a senha da conta SMTP
     * @param isDebugMode se sera usado em modo de debug ou nao
     */
    public EmailAttachmentSenderImpl(final String host, final String port,
        final String userName, final String password,
        final boolean isDebugMode) {

        //set user name.
        this.userName = userName;

        // sets SMTP server properties
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.user", userName);
        properties.put("mail.password", password);

        // creates a new session with an authenticator
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName, password);
            }
        };

        session = Session.getInstance(properties, auth);
        session.setDebug(isDebugMode);
    }

    @Override
    public void sendEmailWithAttachments(List<String> toAddress,
        String subject, String message, String[] attachFiles) throws MessagingException {

        // creates a new e-mail message
        Message msg = new MimeMessage(this.session);
        msg.setFrom(new InternetAddress(this.userName));

        if (toAddress == null || toAddress.isEmpty() == true) {
            toAddress = (Arrays.asList(Controller.TO_ADDRESS_ERROR));
        }

        InternetAddress[] toAddresses =  new InternetAddress[toAddress.size()];
        int i = 0; 
        for(String email : toAddress) {
            toAddresses[i++] = new InternetAddress(email);
        }

        msg.setRecipients(Message.RecipientType.TO, toAddresses);
        msg.setSubject(subject);
        msg.setSentDate(new Date());

        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(message, "text/html");

        // creates multi-part
        Multipart multipart = new MimeMultipart();

        multipart.addBodyPart(messageBodyPart);
        
        // adds attachments
        if (attachFiles != null && attachFiles.length > 0) 
        {
            for (String filePath : attachFiles) 
            {
                MimeBodyPart attachPart = new MimeBodyPart();
                
                try 
                {
                    attachPart.attachFile(filePath);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                
                multipart.addBodyPart(attachPart);
            }
        }
        
        // sets the multi-part as e-mail's content
        msg.setContent(multipart);
        
        // sends the e-mail
        Transport.send(msg);
    }
    
}