package br.com.galgo.notificadordependentes;

import br.com.galgo.notificadordependentes.email.EmailAttachmentSender;
import br.com.galgo.notificadordependentes.email.EmailAttachmentSenderImplNeto;
import br.com.galgo.notificadordependentes.email.EmailSender;
import br.com.galgo.notificadordependentes.email.EmailSenderImplNeto;
import br.com.galgo.notificadordependentes.email.JobExtracaoBaseCompromissoFileUtils;
import br.com.galgo.notificadordependentes.model.JobExtracaoBaseCompromissoBO;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class Controller {

    static Logger logger = LoggerFactory.getLogger(Controller.class);
    private static Controller myInstance;

    private Properties mapOfProperties;
    private static final String RUN_HOMOLOGACAO = "RUN_HOMOLOGACAO";

    private static final String PATH_EMAILS_FILE = "PATH_EMAILS_FILE";
    private static final String PATH_EMAILS_FILE_HOMOL = "PATH_EMAILS_FILE_HOMOL";

    // constantes de configuracao relacionadas ao email
    private static final String HOST = "HOST";

    private static final String PORT = "PORT";
    private static final String USER_NAME = "USER_NAME";
    private static final String PASSWORD = "PASSWORD";
    private static final String IS_MAIL_DEBUGMODE = "IS_MAIL_DEBUGMODE";
    //***********************//


    //***********************//
    // CONSTANTES PUBLICAS   //

    public static final String SUCESS_SUBJECT = "SUCESS_SUBJECT";
    public static final String TEMP_DIR = "TEMP_DIR";
    public static final String ERROR_SEND_EMAILS_TO = "ERROR_SEND_EMAILS_TO";
    public static final String NOME_RELATORIO_AUDITORIA = "NOME_RELATORIO_AUDITORIA";
    public static final String SUBJECT_MAIL_ADDRESS_NOT_FOUND = "SUBJECT_MAIL_ADDRESS_NOT_FOUND";
    public static final String SUBJECT_AUDIT_REPORT = "SUBJECT_AUDIT_REPORT";
    public static final String WAIT_TIME_LOAD_FILE = "WAIT_TIME_LOAD_FILE";
    public static String[] TO_ADDRESS_ERROR;

    /**
     * Construtor pivado, esta classe funcioma como um singleton
     *
     * @author Eduardo.Calderini
     * @throws IOException
     */
    private Controller() throws IOException {
        loadProperties();
    }

    /**
     * Metodo que carrega todas as propriedades definidas no arquvio de config. <br>
     * Se nao conseguir carregar as propriedades, loga o erro e lança excecao. <br>
     *
     * @throws IOException
     *             caso ocorra algum problema.
     * @author eduardo.calderini
     */
    private void loadProperties() throws IOException {
        String propFileName = "config.properties";
        logger.info("Carregando as propriedades do arquivo config.properties do Classpath...");
        try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName)) {

            mapOfProperties = new Properties();

            if (inputStream != null) {
                mapOfProperties.load(inputStream);
            } else {
                throw new FileNotFoundException("Arquivo de property '" + propFileName
                        + "' não foi encontrado no classpath");
            }

            TO_ADDRESS_ERROR = mapOfProperties.getProperty(ERROR_SEND_EMAILS_TO).split(",");

            // log
            logger.info("Configuracoes carregadas com sucesso:\n{}", mapOfProperties.toString().replaceAll(", ", System.getProperty("line.separator")));

        } catch (Exception e) {
            logger.error("Um erro crítico ocorreu e não foi possivel carregar o arquivo de propriedades.", e);
            throw e;
        }
    }

    /**
     * Metodo que retorna a instancia singleton da classe. Ao intanciar o
     * objeto, o arquivo de properties é carregado, caso ocorra algum problema
     * no carregamento o metodo irá abortar a execução do programa.
     *
     * @return uma instancia da classe
     * @author Eduardo.Calderini
     */
    public static Controller getInstance() {
        if (myInstance == null) {
            logger.info("Criando instancia do controller");

            try {
                myInstance = new Controller();
            } catch (IOException e) {
                logger.error("Um erro crítico ocorreu e a aplicação será finalizada.", e);
                System.exit(-1);
            }
        }

        return myInstance;
    }

    /**
     * Metodo que recebe o caminho do arquivo do arquivo a ser processado.
     * Delega o processamento do arqauivo para a classe
     * JobExtracaoBaseCompromissoFileUtils Delega a atualizacao da lista de
     * emails das instituicoes para a classe JobExtracaoBaseCompromissoFileUtils
     * Efetua chamada do metodo sendReports
     *
     * @param file2Process
     *            o Path do arquivo a ser processado.
     * @author Eduardo.Calderini
     * @throws IOException
     */
    public void processJobExtracaoBaseCompromisso(Path file2Process) throws Exception {
        try {
            // limpa o diretorio temporario antes de comecar o processamento.
            cleanTempDir();

            // processa o arquivo do evento monitorado.
            JobExtracaoBaseCompromissoBO.processFile(file2Process);

            // processa o arquivo de emails das instituicoes.
            boolean isRunInHomol = Boolean.parseBoolean(mapOfProperties.getProperty(Controller.RUN_HOMOLOGACAO));
            if (isRunInHomol) {
                JobExtracaoBaseCompromissoFileUtils.readEmailsFile(getProperty(PATH_EMAILS_FILE_HOMOL));
            } else {
                JobExtracaoBaseCompromissoFileUtils.readEmailsFile(getProperty(PATH_EMAILS_FILE));
            }

            // notifica que o relatorio de job será processado
            notificarCriacaoRelatorio(file2Process);

            // dispara os relatorios
            createSendReports();

            // notifica que o relatorio foi processado e enviado com sucesso.
            noficarFimProcessamentoRelatorio(file2Process);

        } catch (IOException | ParseException e) {
            logger.error("Problemas ao processar Extracao Base Comprisso: " + e.getMessage(), e);
            throw e;
        } catch (Exception e) {
            logger.error("Problemas ao processar Extracao Base Comprisso: " + e.getMessage(), e);
            throw e;
        } finally {
            EmailSenderImplNeto.waitEmails();
            EmailAttachmentSenderImplNeto.waitEmails();
        }
    }

    /**
     * Metodo que notifica
     *
     * @param file2Process
     * @throws AddressException
     * @throws MessagingException
     */
    private void notificarCriacaoRelatorio(Path file2Process) throws AddressException, MessagingException {
        String subject = getProperty("EVENTO_IDENTIFICADO_SUBJECT");
        String message = MessageFormat.format(getProperty("EVENTO_IDENTIFICADO_MESSAGE"), file2Process.normalize().toAbsolutePath());
        List<String> toAddress = (Arrays.asList(Controller.TO_ADDRESS_ERROR));
        Controller.getInstance().getEmailSender().sendEmail(toAddress, subject, message);
    }

    private void noficarFimProcessamentoRelatorio(Path file2Process) throws AddressException, MessagingException {
        String subject = "[Sistema Galgo] Processado com Sucesso";
        String message = "O arquivo de Extracao, localizado em " + file2Process.normalize().toAbsolutePath();
        message += "<br><br>Foi processado com sucesso.";
        message += "<br><br>Em instantes o relat&oacute;rio de execu&ccedil;&atilde;o ser&aacute; enviado.";

        // TODO ecalderini: melhor
        List<String> toAddress = (Arrays.asList(Controller.TO_ADDRESS_ERROR));

        Controller.getInstance().getEmailSender().sendEmail(toAddress, subject, message);
    }

    /**
     * Metodo que limpa o diretorio temporario onde a aplicacao salva os
     * arquivos que sao enviados por email. O arquivo temporario e defido por
     * TEMP_DIR
     *
     * @author Eduardo.Calderini
     */
    private void cleanTempDir() {
        File dir = new File(getProperty(TEMP_DIR));

        for (File file : dir.listFiles()) {
            file.delete();
        }
    }

    /**
     * Meotodo que cria uma instancia do gerenciador de relatorios e solitica o
     * envio dos mesmos.
     *
     * @author Eduardo.Calderini
     * @throws Exception
     *             caso haja algum problema.
     */
    private void createSendReports() throws Exception {
        //getProperty(SUCESS_MAIL_MESSAGE)
        final URL url = this.getClass().getResource("/success_mail_message.html");
        new ReportManager(FileUtils.readFileToString(FileUtils.toFile(url), "utf-8")).createSendReports();
    }

    /**
     * Metodo que cria uma instancia do disparador de emails com os devidos
     * parametros configurados.
     *
     * @return mailSender uma instancia de EmailAttachmentSender.
     * @author Eduardo.Calderini
     */
    public EmailAttachmentSender getEmailAttachmentSender() {
        String host = mapOfProperties.getProperty(HOST);
        String port = mapOfProperties.getProperty(PORT);
        String userName = mapOfProperties.getProperty(USER_NAME);
        String password = mapOfProperties.getProperty(PASSWORD);
        boolean isMailDebugMode = Boolean.parseBoolean(mapOfProperties.getProperty(IS_MAIL_DEBUGMODE));

        EmailAttachmentSender mailSender = new EmailAttachmentSenderImplNeto(host, port, userName, password,
            isMailDebugMode);
        //EmailAttachmentSender mailSender = new EmailAttachmentSenderImpl(host, port, userName, password, isMailDebugMode);

        return mailSender;
    }

    /**
     * Metodo que cria uma instancia do disparador de emails com os devidos
     * parametros configurados.
     *
     * @return mailSender uma instancia de EmailAttachmentSender.
     * @author Eduardo.Calderini
     */
    public EmailSender getEmailSender() {
        String host = mapOfProperties.getProperty(HOST);
        String port = mapOfProperties.getProperty(PORT);
        String userName = mapOfProperties.getProperty(USER_NAME);
        String password = mapOfProperties.getProperty(PASSWORD);
        boolean isMailDebugMode = Boolean.parseBoolean(mapOfProperties.getProperty(IS_MAIL_DEBUGMODE));

        EmailSender mailSender = new EmailSenderImplNeto(host, port, userName, password, isMailDebugMode);

        return mailSender;
    }


    public String getProperty(String propertyKey) {
        return mapOfProperties.getProperty(propertyKey);
    }

}