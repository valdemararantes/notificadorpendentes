package br.com.galgo.notificadordependentes.model;

public enum PapelEntidadeComunidoraEnum 
{
    
    ADMINISTRADOR,
    DISTRIBUIDOR,
    GESTOR,
    AUTORREGULACAO,
    CONTROLADOR_DE_ATIVOS,
    CUSTODIANTE,
    CONSUMIDOR,
    CONTROLADOR_DE_PASSIVO,
    AGENTE_DE_CUSTODIA,
    CONSULTORIA,
    AGENTE_AUTONOMO,
    CONTABILIDADE,
    TESOURARIA;
    
    /**
     * 
     * @param str2Convert
     * @return
     */
    public static PapelEntidadeComunidoraEnum getAsEnum(String str2Convert)
    {
        if(str2Convert.equals("ADMINISTRADOR"))
        {
            return ADMINISTRADOR;
        }
        
        if(str2Convert.equals("DISTRIBUIDOR"))
        {
            return DISTRIBUIDOR;
        }
        
        if(str2Convert.equals("GESTOR"))
        {
            return GESTOR;
        }
        
        if(str2Convert.equals("AUTORREGULAÇÃO"))
        {
            return AUTORREGULACAO;
        }
        
        if(str2Convert.equals("CONTROLADOR DE ATIVOS"))
        {
            return CONTROLADOR_DE_ATIVOS;
        }
        
        if(str2Convert.equals("CUSTODIANTE"))
        {
            return CUSTODIANTE;
        }
        
        if(str2Convert.equals("CONSUMIDOR"))
        {
            return CONSUMIDOR;
        }
        
        if(str2Convert.equals("CONTROLADOR DE PASSIVO"))
        {
            return CONTROLADOR_DE_PASSIVO;
        }
        
        if(str2Convert.equals("AGENTE DE CUSTÓDIA"))
        {
            return AGENTE_DE_CUSTODIA;
        }
        
        if(str2Convert.equals("CONSULTORIA"))
        {
            return CONSULTORIA;
        }
        
        if(str2Convert.equals("AGENTE AUTÔNOMO"))
        {
            return AGENTE_AUTONOMO;
        }
        
        if(str2Convert.equals("CONTABILIDADE"))
        {
            return CONTABILIDADE;
        }
        
        if(str2Convert.equals("TESOURARIA"))
        {
            return TESOURARIA;
        }

        return null;
    }
}
