package br.com.galgo.notificadordependentes.model;

public enum TipoCompromissoEnum {
    SEMI_AUTOMATICO,
    AUTOMATICO,
    MANUAL;
    
    /**
     * 
     * @param str2Convert
     * @return
     */
    public static TipoCompromissoEnum getAsEnum(String str2Convert)
    {
        if(str2Convert.equals("SEMI_AUTOMATICO"))
        {
            return SEMI_AUTOMATICO;
        }

        if(str2Convert.equals("AUTOMATICO"))
        {
            return AUTOMATICO;
        }

        if(str2Convert.equals("MANUAL"))
        {
            return MANUAL;
        }
        
        return null;
    }
}
