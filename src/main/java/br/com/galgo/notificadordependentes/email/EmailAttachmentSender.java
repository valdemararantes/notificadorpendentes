package br.com.galgo.notificadordependentes.email;

import javax.mail.MessagingException;
import java.util.List;

/**
 * Created by Valdemar.Arantes on 12/08/2016.
 */
public interface EmailAttachmentSender {
    void sendEmailWithAttachments(List<String> toAddress,
        String subject, String message, String[] attachFiles) throws MessagingException;
}
