package br.com.galgo.notificadordependentes;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileFilter;

public class Main {

    private final static Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        try {
            File compromissosDir = new File("./compromissos");
            FileFilter csvFilter = new SuffixFileFilter(".csv");
            final File[] files = compromissosDir.listFiles(csvFilter);
            if (files == null || files.length == 0) {
                log.info("Nenhum arquivo de compromisso para processar encontrado na pasta {}",
                    compromissosDir.getCanonicalPath());
                return;
            }
            log.info("Processando o arquivo de compromisso {}", files[0].getCanonicalPath());
            Controller.getInstance().processJobExtracaoBaseCompromisso(files[0].toPath());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return;
        }
    }
}